package com.exesandohs;

public class ExesAndOhs {

    public boolean checkXO(String input) {
        char[] charArray = input.toCharArray();
        int countX=0, countY=0;
        for (int i = 0; i <charArray.length ; i++) {

            if(charArray[i]=='x'|| charArray[i]=='X')
                countX++;

            if(charArray[i]=='o'|| charArray[i]=='O')
                countY++;

        }

        if(countX==countY)
            return true;
        else
            return false;
    }

}
